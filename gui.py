'''
from guizero import App

app = App(title="Qubix", height=600, width=800)
app.title = "Qubix"
app.display()
'''


'''
from guizero import App, Text

# Action you would like to perform
def counter():
    text.value = int(text.value) + 1

app = App(title="Qubix", height=600, width=800)
text = Text(app, text="1")
text.repeat(1000, counter)  # Schedule call to counter() every 1000ms
app.display()
'''



from guizero import *

app = App(title="Qubix", height=600, width=800) # , layout='grid')




def run_feed():
    print("Running feed")
    text = Text(app, text="Hello World")
    picture.hide()



def edit_function():
    print("Edit option")
    picture.hide()


def do_this_on_close():
    if yesno("Close", "Are you sure you want to quit?"):
        app.destroy()


menubar = MenuBar(app,
                  toplevel=["Feed", "Trade", "Settings"],
                  options=[
                      [ ["Run", run_feed], ["Pause", run_feed] ],
                      [ ["Live", edit_function], ["Demo", edit_function] ],
                      [ ["Strategy", edit_function], ["User", edit_function] ]
                  ])


picture = Picture(app, image='logo.gif') #, grid = [0,0])


app.on_close(do_this_on_close)


# button = PushButton(app, command=do_nothing)
app.display()

